import { createTheme } from '@mui/material/styles';

// Create a theme instance.
export const theme = createTheme({
  palette: {
    primary: {
      main: '#2e4850',
      bg: '#60818A',
      button: '#B9DCE7',
      add: '#36595F',
    },
  },
  breakpoints: {
    values: {
      xs: 380, // Extra small devices (e.g., phones)
      sm: 576, // Small devices (e.g., tablets)
      md: 768, // Medium devices (e.g., laptops)
      lg: 992, // Large devices (e.g., desktops)
      xl: 1200, // Extra large devices
    },
  },
});
