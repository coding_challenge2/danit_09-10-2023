import { Box, Button, styled } from '@mui/material';
import { useDispatch } from 'react-redux';
import { clearLog, clearQueue, setQueue } from '../store/reducers';

const ButtonBox = styled(Box)(({ theme }) => ({
  margin: '6px',
  padding: '20px',
  backgroundColor: theme.palette.primary.bg,
  display: 'grid',
  justifyItems: 'center',
  justifyContent: 'center',
  gridTemplateColumns: 'repeat(4, auto)',
  [theme.breakpoints.down('md')]: {
    gridTemplateColumns: 'repeat(2, auto)',
  },
  [theme.breakpoints.down('xs')]: {
    gridTemplateColumns: 'repeat(1, 1fr)',
  },
  gap: '10px',
}));
const CustomBtn = styled(Button)(({ theme }) => ({
  width: '125px',
  minWidth: '125px',
  whiteSpace: 'nowrap',
  textTransform: 'none',
  [theme.breakpoints.down('xs')]: {
    width: '240px',
    minWidth: '240px',
  },
}));

export const ButtonsPanel = () => {
  // // redux timer data
  // const { logRedux, isTimerRunning, queue } = useSelector(
  //   (state) => state.timers
  // );
  const dispatch = useDispatch();

  // timer button click
  const handleTimerBtnClick = (buttonNumber, delay) => {
    const nowDate = new Date();
    const clickTime = nowDate.toISOString();
    // add an object to the queue array
    dispatch(setQueue({ buttonNumber, delay, clickTime }));
  };

  // clear button click
  const handleClearBtnClick = () => {
    dispatch(clearQueue());
    dispatch(clearLog());
  };

  return (
    <ButtonBox>
      <CustomBtn variant="contained" onClick={() => handleTimerBtnClick(1, 1)}>
        Start Timer 1s
      </CustomBtn>
      <CustomBtn variant="contained" onClick={() => handleTimerBtnClick(2, 2)}>
        Start Timer 2s
      </CustomBtn>
      <CustomBtn variant="contained" onClick={() => handleTimerBtnClick(3, 3)}>
        Start Timer 3s
      </CustomBtn>
      <CustomBtn variant="contained" onClick={handleClearBtnClick}>
        Clear
      </CustomBtn>
    </ButtonBox>
  );
};
