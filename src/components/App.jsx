import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container } from '@mui/material';

// redux
import {
  setLog,
  setIsTimerRunning,
  removeFirstElmQueue,
} from '../store/reducers';

// components
import { ButtonsPanel } from './ButtonsPanel';
import { LogPanel } from './LogPanel';
import { ProgressPanel } from './ProgressPanel';

export const App = () => {
  // redux timer data
  const { isTimerRunning, queue } = useSelector((state) => state.timers);
  const dispatch = useDispatch();

  // timer
  const startTimer = () => {
    dispatch(setIsTimerRunning(true));
    const currentTask = queue[0];
    const clickTime = new Date(currentTask.clickTime);

    // run timer
    setTimeout(() => {
      const logTime = new Date();
      const passedTime = (logTime - clickTime) / 1000;

      // TODO: delete
      console.log(`btn-${currentTask.buttonNumber}`);

      // set redux log
      const logEntry = `Button №${
        currentTask.buttonNumber
      }: clickBtnTimer=${clickTime.toLocaleTimeString()} - writeLog=${logTime.toLocaleTimeString()} (${passedTime.toFixed(
        2
      )} sec)`;
      dispatch(setLog(logEntry));

      // end timer / delete first obj
      dispatch(setIsTimerRunning(false));
      dispatch(removeFirstElmQueue());
    }, currentTask.delay * 1000);
  };

  // start timer
  useEffect(() => {
    if (queue.length > 0 && !isTimerRunning) {
      startTimer();
    }

    // TODO: delete
    console.log('queue', queue);
    //
  }, [queue]);

  return (
    <Container sx={{ minWidth: '320px' }}>
      <ButtonsPanel />
      <ProgressPanel />
      <LogPanel />
    </Container>
  );
};
