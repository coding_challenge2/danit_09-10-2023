import { useSelector } from 'react-redux';
import { Box, Typography, styled, useTheme } from '@mui/material';

const ProgressBox = styled(Box)(({ theme }) => ({
  margin: '6px',
  padding: '10px',
  minHeight: '58px',
  backgroundColor: theme.palette.primary.bg,
  display: 'flex',
  justifyContent: 'flex-start',
  alignItems: 'center',
  // gap: '10px',
}));

const ProgressItems = styled(Box)(() => ({
  display: 'flex',
  justifyContent: 'flex-start',
  flexWrap: 'wrap',
  gap: '6px',
}));

const ItemQueue = styled(Box)(({ theme }) => ({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  border: `2px solid ${theme.palette.primary.add}`,
  borderRadius: '50%',
  height: '44px',
  width: '44px',
}));

export const ProgressPanel = () => {
  // redux timer data
  const { queue } = useSelector((state) => state.timers);

  const theme = useTheme();

  return (
    <ProgressBox>
      {!queue.length ? (
        <Typography sx={{ margin: '0 auto' }}>No progress</Typography>
      ) : (
        <ProgressItems>
          {queue.map(({ delay, clickTime }, index) => (
            <ItemQueue
              key={clickTime}
              sx={{
                borderColor:
                  index === 0 ? '#974464' : theme.palette.primary.add,
              }}
            >
              {`${delay}s`}
            </ItemQueue>
          ))}
        </ProgressItems>
      )}
    </ProgressBox>
  );
};
