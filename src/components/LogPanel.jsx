import { useSelector } from 'react-redux';
import { Box, Typography, styled } from '@mui/material';

const LogBox = styled(Box)(({ theme }) => ({
  margin: '6px',
  padding: '20px',
  backgroundColor: theme.palette.primary.bg,
  display: 'flex',
  justifyContent: 'flex-start',
  // gap: '10px',
}));

export const LogPanel = () => {
  // redux timer data
  const { logRedux } = useSelector((state) => state.timers);

  return (
    <LogBox>
      {!logRedux.length ? (
        <Typography sx={{ margin: '0 auto' }}>Log list is empty</Typography>
      ) : (
        <ul>
          {logRedux.map((logText, index) => (
            <li key={index}>{logText}</li>
          ))}
        </ul>
      )}
    </LogBox>
  );
};
