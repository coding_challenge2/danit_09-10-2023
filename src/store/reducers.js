import { createSlice } from '@reduxjs/toolkit';

// initialization default state
const initialState = {
  logRedux: JSON.parse(localStorage.getItem('logRedux')) || [],
  queue: [],
  isTimerRunning: false,
};

const TimersSlice = createSlice({
  name: 'timers',
  initialState,
  reducers: {
    setLog: (state, action) => {
      state.logRedux = [...state.logRedux, action.payload];
      localStorage.setItem('logRedux', JSON.stringify(state.logRedux));
    },
    clearLog: (state) => {
      state.logRedux = [];
      localStorage.removeItem('logRedux');
    },
    setIsTimerRunning: (state, action) => {
      state.isTimerRunning = action.payload;
    },
    setQueue: (state, action) => {
      state.queue = [...state.queue, action.payload];
    },
    clearQueue: (state) => {
      state.queue = [];
    },
    removeFirstElmQueue: (state) => {
      state.queue = state.queue.slice(1);
    },
  },
});

export const {
  setLog,
  clearLog,
  setIsTimerRunning,
  setQueue,
  clearQueue,
  removeFirstElmQueue,
} = TimersSlice.actions;
export default TimersSlice.reducer;
