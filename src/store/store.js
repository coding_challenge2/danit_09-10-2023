import { configureStore } from '@reduxjs/toolkit';
import timers from './reducers';

export const store = configureStore({
  reducer: {
    timers,
  },
});
